<?php
    use Vale\Read\Read as Read;
    require_once __DIR__.'/api/start.php';

    $productos = new Read();
    $productos->search( $_GET['search'] );
    echo $productos->getResponse();
?>