<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro de Productos</title>
        <style type="text/css">
			body {margin: 20px; 
			background-color: pink;
			font-family: Verdana, Helvetica, sans-serif;
			font-size: 90%;}
			h1 {color: #005825;
			border-bottom: 1px solid #005825;}
			h2 {font-size: 1.2em;
			color: #380136;}
		</style>
</head>
<body>
<form  action="set_producto_v3.php" method="post">

<h2>Agregar Producto Nuevo</h2>

  <fieldset>
    <legend>Informacion de Producto</legend>

    <ul>
      <li><label>ID:</label> <input type="text" name="id" value="<?= !empty($_POST['id'])?$_POST['id']:$_GET['id'] ?>">  </li>
      <li><label>Nombre:</label> <input type="text" name="name" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>">  </li>
      <li><label for="form-marca">Marca:</label><input type="text" name="marca" id="form-marca" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>"></li>
      <li><label for="form-modelo">Modelo:</label><input type="text" name="modelo" id="form-modelo" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>"></li>
      <li><label for="form-precio">Precio:</label> <input type="number" name="precio" id="form-precio" min="1.00" step="0.01"  pattern="[0-9]{1,9}" value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>"></li>
      <li><label for="form-detalles">Detalles:</label><br/><textarea name="detalles" rows="4" cols="60" id="form-detalles" placeholder="No más de 250 caracteres de longitud"  maxlength="250" value=""><?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?></textarea></li>
      <li><label for="form-unidades">Unidades:</label><input type="number" name="unidades" id="form-unidades" min="1" step="1"pattern="[0-9]{1,9}" value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>"></li>
      <li><label for="form-imagen">Imagen:</label><input type="text" name="imagen" id="form-imagen" placeholder="img/imagen-ejemplo1.png" value="<?= !empty($_POST['imagen'])?$_POST['imagen']:$_GET['imagen'] ?>"></li>
    </ul>
  </fieldset>
  <p>
    <input type="submit" value="Actualizar"/>
    <input type="reset"/>
  </p>
</form> 
    </form>
</body>
</html>